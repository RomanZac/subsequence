package com;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Subsequence<T> {

	ArrayList<T> temp = new ArrayList<T>();

	public boolean find(List<T> A, List<T> B) {
		try {
			if (A == null || B == null)
				throw new NullPointerException();
			if ((A.isEmpty() && !B.isEmpty()) || (A.isEmpty() && B.isEmpty()))
				return true;
			if (B.isEmpty() || !B.contains(A.get(0)) || B.size() < A.size())
				return false;
		} catch (Exception ex) {
			return false;
		}

		Iterator<T> itA = A.iterator();
		T tmp = itA.next();
		int num = B.indexOf(tmp);
		tmp = itA.next();
		while (itA.hasNext()) {
			if (B.contains(tmp) && num < B.indexOf(tmp)) {
				num = B.indexOf(tmp);
				tmp = itA.next();
			} else
				return false;
		}
		return true;
	}
}